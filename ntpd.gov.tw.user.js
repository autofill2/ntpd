// ==UserScript==
// @name        ntpd.gov.tw
// @namespace   autofill
// @match       https://tvrs.ntpd.gov.tw/Home/Report_Add
// @grant       none
// @version     1.1.0
// @updateURL	https://gitlab.com/autofill2/ntpd/-/raw/main/ntpd.gov.tw.user.js?inline=false
// @downloadURL	https://gitlab.com/autofill2/ntpd/-/raw/main/ntpd.gov.tw.user.js?inline=false
// @author      -
// @description 新北市政府警察局,交通違規檢舉系統, 自動填入檢舉人資料
// ==/UserScript==

let profile = {name: "你的姓名", id: "你的身份証字號", address:"你的地址", email:"你的電子郵件", phone:"你的電話"};

document.getElementById("informerData_informer_name").value=profile.name;
document.getElementById("informerData_identity").value=profile.id
document.getElementById("informerData_contact_address").value=profile.address;
document.getElementById("informerData_Email").value=profile.email;
document.getElementById("informerData_Phone").value=profile.phone;